import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePassComponent } from './site/dashboard/change-pass/change-pass.component';
import { HomeComponent } from './site/dashboard/home/home.component';
import { LoginComponent } from './site/dashboard/login/login.component';
import { ProfileComponent } from './site/dashboard/profile/profile.component';
import { RegisterComponent } from './site/dashboard/register/register.component';
import { SiteComponent } from './site/site.component';

const routes: Routes = [
  { path: '', 
    component: SiteComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'login', component: LoginComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'change-pass', component: ChangePassComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
