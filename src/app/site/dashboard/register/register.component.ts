import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/shared/interfaces/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userRegister = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    pass: ['', Validators.required],
    name: ['', Validators.required],
    phone: ['', Validators.required],
  });

  error: String = '';
  success: boolean = false;
  showSpinner: boolean = false;

  usersCollection = this.afDB.collection<User>('users');
  userData: User = null;

  constructor(
    private formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public afDB: AngularFirestore,
  ) { }

  ngOnInit(): void {
  }

  onRegister() {
    if (this.userRegister.valid) {
      let email = this.userRegister.get('email').value;
      let pass = this.userRegister.get('pass').value;

      this.userData = {
        name: this.userRegister.get('name').value,
        phone: this.userRegister.get('phone').value,
        email: this.userRegister.get('email').value,
      }
      this.error = '';
      this.showSpinner = true;
      this.afAuth.createUserWithEmailAndPassword(email, pass)
      .then((result) => {
        this.usersCollection.doc(result.user.uid).set(this.userData).then(
          (result) => {
            this.showSpinner = false;
            this.success = true;
          }),
          (error) => {
            this.showSpinner = false;
            this.success = false;
            this.error = error.message;
          }
      })
      .catch((error) => {
        this.showSpinner = false;
        this.success = false;
        this.error = error.message;
      })
    }
  }
}
