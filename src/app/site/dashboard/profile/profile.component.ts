import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userData = null;
  constructor(
    public afDB: AngularFirestore,
  ) { }

  // TODO: Agregar el ID del documento de la persona logueada
  // TODO: Informar que no existe el usuario
  // TODO: Informar si hubo un error en la busqueda
  // TODO: Informar que esta buscando los datos
  ngOnInit(): void {
    this.afDB.collection('users').doc("JnTc9K9dQnMuxzrKkTc48PJ9Pn73").ref.get().then((doc) => {
      if (doc.exists) {
        this.userData = doc.data();
      } else {
        console.log("No existe el usuario");
      }
    }).catch((error) => {
      console.log("Ocurrio un error", error);
    });
  }

  getList() {
    return Object.keys(this.userData);
  }

  getValue(key) {
    return this.userData[key];
  }
}

