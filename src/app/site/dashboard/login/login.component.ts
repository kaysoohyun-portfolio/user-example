import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error: string = '';
  success: boolean = false;
  showSpinner: boolean = false;
  hide: boolean = true;
  userlogin = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    pass: ['', Validators.required],
  });
  constructor(
    private formBuilder: FormBuilder,
    private afAuth: AngularFireAuth,
  ) { }

  ngOnInit(): void {
  }


  onLogin() {
    if (this.userlogin.valid) {
      let email = this.userlogin.get('email').value;
      let pass = this.userlogin.get('pass').value;
      this.error = '';
      this.showSpinner = true;
      this.afAuth.signInWithEmailAndPassword(email, pass)
      .then((result) => {
        this.showSpinner = false;
        this.success = true;
      })
      .catch((error) => {
        this.showSpinner = false;
        this.success = false;
        this.error = error.message;
      });
    }
  }

  getIconPass() {
    return this.hide ? 'fa fa-eye-slash' : 'fa fa-eye';
  }

}
