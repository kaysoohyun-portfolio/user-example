import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit {
  userData = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
  });
  error: string = '';
  showSpinner: boolean = false;
  success: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private afAuth: AngularFireAuth,
  ) { }

  ngOnInit(): void {
  }

  onRecoveryPass() {
    let email = this.userData.get('email').value;
    if (this.userData.valid) {
      this.error = '';
      this.showSpinner = true;
      this.afAuth.sendPasswordResetEmail(email)
      .then((result) => {
        this.showSpinner = false;
        this.success = true;
      })
      .catch((error) => {
        this.showSpinner = false;
        this.error = error.message;
      });
    }
  }
}
