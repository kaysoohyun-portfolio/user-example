import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-panel',
  templateUrl: './nav-panel.component.html',
  styleUrls: ['./nav-panel.component.scss']
})
export class NavPanelComponent implements OnInit {
  sectionList = [
    { name: 'Home', route: '/' },
    { name: 'Login', route: '/login' },
    { name: 'Register', route: '/register' },
    { name: 'Recovery Password', route: '/change-pass' },
    { name: 'Profile', route: '/profile' },
  ];
  constructor(
    private router: Router,
  ) { }

  // TODO: Logout
  ngOnInit(): void {
  }

  onNavigate(route) {
    this.router.navigate([route], { queryParamsHandling: 'merge' });
  }

}
