import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SiteComponent } from './site/site.component';
import { NavPanelComponent } from './site/nav-panel/nav-panel.component';
import { DashboardComponent } from './site/dashboard/dashboard.component';
import { LoginComponent } from './site/dashboard/login/login.component';
import { RegisterComponent } from './site/dashboard/register/register.component';
import { ChangePassComponent } from './site/dashboard/change-pass/change-pass.component';
import { ProfileComponent } from './site/dashboard/profile/profile.component';
import { HomeComponent } from './site/dashboard/home/home.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';

import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    SiteComponent,
    NavPanelComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    ChangePassComponent,
    ProfileComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatIconModule,
  ],
  providers: [AngularFireAuthModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
